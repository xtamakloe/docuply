# A full example with AWS credentials:
#     CarrierWave.configure do |config|
#       config.fog_credentials = {
#         :aws_access_key_id => 'xxxxxx',
#         :aws_secret_access_key => 'yyyyyy',
#         :provider => 'AWS'
#       }
#       config.fog_directory = 'directoryname'
#       config.fog_public = true
#     end
#
#
CarrierWave.configure do |config|
  config.fog_credentials = {
    provider: 'AWS', # required
    aws_access_key_id:     ENV['AWS_ACCESS_KEY_ID'],
    aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
    region:                ENV['AWS_REGION'],
    use_iam_profile: false, # optional, defaults to false
    # host: 's3.example.com', # optional, defaults to nil
    # endpoint: 'https://s3.example.com:8080' # optional, defaults to nil
  }
  config.fog_directory   = ENV['AWS_BUCKET']
  config.fog_public      = false # optional, defaults to true
  config.fog_attributes  = { cache_control: "public, max-age=#{365.days.to_i}" } # optional, defaults to {}
end