Rails.application.routes.draw do

  devise_for :users, controllers: {
      sessions: 'auth/users/sessions',
      registrations: 'auth/users/registrations'
  }

  scope module: 'app' do
    resource :user, only: [:show, :edit] do
      collection do
        patch 'update_password'
      end
    end
    resources :projects do
      resources :entity_types do
        resources :entity_type_attributes
        resources :templates
        resources :operations do
          resources :downloads, only: [:create]
        end
      end
    end
    match '/help', to: 'pages#help', via: :get, as: :help
  end

  # https://hemamca.wordpress.com/2015/03/02/redirecting-to-custom-404-and-500-pages-in-rails/
  match '/404', to: 'app/pages#file_not_found', via: :all
  match '/422', to: 'app/pages#unprocessable', via: :all
  match '/500', to: 'app/pages#internal_server_error', via: :all


  # root to: 'app/pages#home'
  root to: 'app/projects#index'
end

