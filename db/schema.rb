# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_19_233408) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "entities", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "entity_type_id", null: false
    t.bigint "operation_id", null: false
    t.index ["entity_type_id"], name: "index_entities_on_entity_type_id"
    t.index ["operation_id"], name: "index_entities_on_operation_id"
    t.index ["project_id"], name: "index_entities_on_project_id"
  end

  create_table "entity_attributes", force: :cascade do |t|
    t.bigint "entity_id", null: false
    t.bigint "entity_type_attribute_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "value"
    t.index ["entity_id"], name: "index_entity_attributes_on_entity_id"
    t.index ["entity_type_attribute_id"], name: "index_entity_attributes_on_entity_type_attribute_id"
  end

  create_table "entity_type_attributes", force: :cascade do |t|
    t.bigint "entity_type_id", null: false
    t.string "name"
    t.string "var_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "hidden", default: false
    t.index ["entity_type_id"], name: "index_entity_type_attributes_on_entity_type_id"
  end

  create_table "entity_types", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["project_id"], name: "index_entity_types_on_project_id"
  end

  create_table "operations", force: :cascade do |t|
    t.bigint "project_id", null: false
    t.bigint "entity_type_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.json "operation_data_file"
    t.string "notes"
    t.boolean "custom_filenames", default: false
    t.index ["entity_type_id"], name: "index_operations_on_entity_type_id"
    t.index ["project_id"], name: "index_operations_on_project_id"
  end

  create_table "operations_templates", id: false, force: :cascade do |t|
    t.bigint "operation_id", null: false
    t.bigint "template_id", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "account_id", null: false
    t.string "title"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.text "description"
    t.index ["account_id"], name: "index_projects_on_account_id"
  end

  create_table "templates", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "entity_type_id", null: false
    t.string "file_name"
    t.json "template_file"
    t.text "description"
    t.string "name"
    t.boolean "text_only", default: false
    t.string "all_placeholders_list", default: ""
    t.string "registered_placeholders_list", default: ""
    t.index ["entity_type_id"], name: "index_templates_on_entity_type_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "username", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.bigint "account_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "first_name"
    t.string "last_name"
    t.index ["account_id"], name: "index_users_on_account_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "entities", "entity_types"
  add_foreign_key "entities", "operations"
  add_foreign_key "entities", "projects"
  add_foreign_key "entity_attributes", "entities"
  add_foreign_key "entity_attributes", "entity_type_attributes"
  add_foreign_key "entity_type_attributes", "entity_types"
  add_foreign_key "entity_types", "projects"
  add_foreign_key "operations", "entity_types"
  add_foreign_key "operations", "projects"
  add_foreign_key "projects", "accounts"
  add_foreign_key "templates", "entity_types"
  add_foreign_key "users", "accounts"
end
