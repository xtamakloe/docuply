class RemoveVarNameFromEntityAttributes < ActiveRecord::Migration[6.0]
  def change

    remove_column :entity_attributes, :var_name, :string
  end
end
