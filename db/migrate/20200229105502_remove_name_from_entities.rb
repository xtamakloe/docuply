class RemoveNameFromEntities < ActiveRecord::Migration[6.0]
  def change

    remove_column :entities, :name, :string
  end
end
