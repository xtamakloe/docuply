class AddEntityToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_reference :templates, :entity, null: false, foreign_key: true
  end
end
