class AddEntityTypeToEntities < ActiveRecord::Migration[6.0]
  def change
    add_reference :entities, :entity_type, null: false, foreign_key: true
  end
end
