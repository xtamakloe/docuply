class AddOperationDataFileToOperations < ActiveRecord::Migration[6.0]
  def change
    add_column :operations, :operation_data_file, :json
  end
end
