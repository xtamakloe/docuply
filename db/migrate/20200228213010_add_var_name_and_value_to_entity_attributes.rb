class AddVarNameAndValueToEntityAttributes < ActiveRecord::Migration[6.0]
  def change
    add_column :entity_attributes, :var_name, :string
    add_column :entity_attributes, :value, :string
  end
end
