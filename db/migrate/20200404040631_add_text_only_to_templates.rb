class AddTextOnlyToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :templates, :text_only, :boolean, default: false
  end
end
