class AddNotesToOperations < ActiveRecord::Migration[6.0]
  def change
    add_column :operations, :notes, :string
  end
end
