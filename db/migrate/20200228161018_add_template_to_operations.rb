class AddTemplateToOperations < ActiveRecord::Migration[6.0]
  def change
    add_reference :operations, :template, null: false, foreign_key: true
  end
end
