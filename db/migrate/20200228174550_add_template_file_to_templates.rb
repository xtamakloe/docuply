class AddTemplateFileToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :templates, :template_file, :json
  end
end
