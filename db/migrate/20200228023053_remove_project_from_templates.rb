class RemoveProjectFromTemplates < ActiveRecord::Migration[6.0]
  def change
    remove_reference :templates, :project, null: false, foreign_key: true
  end
end
