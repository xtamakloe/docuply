class CreateOperations < ActiveRecord::Migration[6.0]
  def change
    create_table :operations do |t|
      t.references :project, null: false, foreign_key: true
      t.references :entity_type, null: false, foreign_key: true

      t.timestamps
    end
  end
end
