class CreateEntityAttributes < ActiveRecord::Migration[6.0]
  def change
    create_table :entity_attributes do |t|
      t.references :entity, null: false, foreign_key: true
      t.references :entity_type_attribute, null: false, foreign_key: true

      t.timestamps
    end
  end
end
