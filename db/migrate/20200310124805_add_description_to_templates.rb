class AddDescriptionToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :templates, :description, :text
  end
end
