class AddFileNameToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :templates, :file_name, :string
  end
end
