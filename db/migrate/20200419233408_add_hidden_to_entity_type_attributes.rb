class AddHiddenToEntityTypeAttributes < ActiveRecord::Migration[6.0]
  def change
    add_column :entity_type_attributes, :hidden, :boolean, default: false
  end
end
