class CreateJoinTableOperationsTemplates < ActiveRecord::Migration[6.0]
  def change
    create_join_table :operations, :templates do |t|
      # t.index [:operation_id, :template_id]
      # t.index [:template_id, :operation_id]
    end
  end
end
