class AddOperationToEntities < ActiveRecord::Migration[6.0]
  def change
    add_reference :entities, :operation, null: false, foreign_key: true
  end
end
