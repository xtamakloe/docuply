class AddPlaceholdersListsToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_column :templates, :all_placeholders_list, :string, default: ""
    add_column :templates, :registered_placeholders_list, :string, default: ""
  end
end
