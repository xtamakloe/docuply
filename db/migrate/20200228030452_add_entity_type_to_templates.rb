class AddEntityTypeToTemplates < ActiveRecord::Migration[6.0]
  def change
    add_reference :templates, :entity_type, null: false, foreign_key: true
  end
end
