class AddCustomFilenamesToOperations < ActiveRecord::Migration[6.0]
  def change
    add_column :operations, :custom_filenames, :boolean, default: false
  end
end
