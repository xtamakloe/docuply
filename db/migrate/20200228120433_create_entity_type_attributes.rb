class CreateEntityTypeAttributes < ActiveRecord::Migration[6.0]
  def change
    create_table :entity_type_attributes do |t|
      t.references :entity_type, null: false, foreign_key: true
      t.string :name
      t.string :var_name

      t.timestamps
    end
  end
end
