module ApplicationHelper
  #TODO: move to app/
  def bootstrap_class_for flash_type
    case flash_type.to_s.to_sym
    when :success
      "alert-success"
    when :error
      "alert-danger"
    when :alert
      "alert-warning"
    when :notice
      "alert-info"
    else
      flash_type.to_s
    end
  end


  def for?(controller, action = nil)
    if params[:controller] == "app/#{controller}"
      return params[:action] == action.to_s if action
      true
    else
      false
    end
  end


  def edit_or_new_page?
    %w(edit new).include?(request.fullpath.split('/').last)
  end
end
