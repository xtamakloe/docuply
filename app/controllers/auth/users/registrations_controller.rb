class Auth::Users::RegistrationsController < ApplicationController
  layout :set_layout


  def new
    @user = User.new
  end


  def create
    @user = User.new(user_params)

    error_messages = []

    if @user.email
      if User.where(email: @user.email).first
        error_messages << 'Could not create account! Email already taken.'
      else
        account = Account.new(name: @user.email.gsub('@', '').gsub('.', ''))
        if account.save
          @user.account = account
        else
          error_messages << 'Could not create account! Email already taken'
        end
      end
    end

    if error_messages.empty? && @user.save
      sign_in @user

      flash[:success] = "Welcome!"

      redirect_to projects_path
    else
      account.destroy if account

      if error_messages.empty?
        error_messages << @user.errors.full_messages
      end

      flash.now[:alert] = error_messages.flatten.first
      render 'new'
    end
  end


  private


  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end

  def set_layout
    case action_name
    when 'new', 'create'
      'sessions'
    when 'show', 'edit', 'update'
      'app'
    else
      nil
    end
  end
end
