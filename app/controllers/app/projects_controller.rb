class App::ProjectsController < App::BaseController
  before_action :find_project, only: [:show, :edit, :update, :destroy]


  def index
    @projects = current_account.projects
  end


  def show
  end


  def new
    @project = current_account.projects.new
  end


  def edit
    render 'new'
  end


  def create
    @project = current_account.projects.new(project_params)
    if @project.save
      flash[:success] = "Document added!"
      redirect_to @project
    else
      flash.now[:error] = @project.errors.full_messages.first
      render 'new'
    end
  end


  def update
    if @project.update_attributes(project_params)
      flash[:success] = "Updated!"
      redirect_to @project
    else
      flash.now[:error] = @project.errors.full_messages.first
      render 'new'
    end
  end


  def destroy
    if @project && @project.destroy
      flash[:success] = "Deleted!"
      redirect_to projects_path
    end
  end


  private


  def project_params
    params.require(:project).permit(:title, :description)
  end


  def find_project
    @project = current_account.projects.find(params[:id])
  end
end
