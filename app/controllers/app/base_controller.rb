class App::BaseController < ApplicationController
  before_action :authenticate_user!

  layout 'app'

  helper_method :current_account

  def current_account
    @current_account ||= current_user.try(:account)
  end

end

