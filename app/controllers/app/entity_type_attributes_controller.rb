class App::EntityTypeAttributesController < App::BaseController

  before_action :load_entity_type


  def new
    @entity_type_attribute = @entity_type.entity_type_attributes.new
  end


  def edit
    @entity_type_attribute = @entity_type.entity_type_attributes.find(params[:id])
    render 'new'
  end


  def update
    @entity_type_attribute = @entity_type.entity_type_attributes.find(params[:id])
    if @entity_type_attribute.update_attributes(attribute_params)
      flash[:success] = "Updated!"
      redirect_to [@project, @entity_type]
    else
      flash[:error] = "Failed to update!"
      render 'new'
    end

  end


  def create
    attributes_string = params[:attributes_string]

    if attributes_string.present?
      errors = []

      attributes_string.split.each do |attribute|
        eat = EntityTypeAttribute.new(
          var_name:    attribute.strip,
          entity_type: @entity_type
        )

        errors << attribute unless eat.save
      end

      if errors.present?
        flash[:error] = "Error! #{errors.size} not added!"
      else
        flash[:success] = 'Attributes added'
      end

      redirect_to [@project, @entity_type]
    elsif attribute_params
      @entity_type_attribute = @entity_type.entity_type_attributes.new(attribute_params)
      if @entity_type_attribute.save
        flash[:success] = "Added!"
        redirect_to [@project, @entity_type]
      else
        flash[:error] = @entity_type_attribute.errors.full_messages
        render 'new'
      end

    else
      flash[:error] = "No attributes provided"
      render :new
    end
  end


  def destroy
    @entity_type_attribute = @entity_type.entity_type_attributes.find(params[:id])
    if @entity_type_attribute.destroy
      flash[:success] = "Deleted!"
    end
    redirect_to [@project, @entity_type]
  end


  private


  def attribute_params
    params.require(:entity_type_attribute).permit(
      :name, :var_name, :attributes_string
    )
  end


  def load_entity_type
    @project     = current_account.projects.find(params[:project_id])
    @entity_type = @project.entity_types.find(params[:entity_type_id])
  end
end
