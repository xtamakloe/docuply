class App::TemplatesController < App::BaseController

  before_action :load_entity_type


  def index
  end


  def new
    @template = @templates.new
  end


  def edit
    @template = @templates.find(params[:id])
    render 'new'
  end


  def create
    @template = @templates.new(template_params)
    if @template.save
      flash[:success] = "Added!"
      redirect_to_templates_path
    else
      flash[:error] = @template.errors.full_messages.first.gsub!('Template file', '')
      render 'new'
    end
  end


  def update
    @template = @templates.find(params[:id])
    if @template.update_attributes(template_params)
      flash[:success] = "Template updated!"
      redirect_to_templates_path
    else
      flash[:error] = @template.errors.full_messages.first.gsub!('Template file', '')
      render 'new'
    end
  end


  def destroy
    @template = @templates.find(params[:id])
    if @template.destroy
      flash[:success] = "Template removed!"
    end
    redirect_to_templates_path
  end

  private


  def load_entity_type
    @project     = current_account.projects.find(params[:project_id])
    @entity_type = @project.sole_entity_type
    @templates   = @entity_type.templates
  end


  def template_params
    params.require(:template).permit(
      :name,
      :description,
      :template_file,
      :text_only
    )
  end

  def redirect_to_templates_path
    redirect_to project_entity_type_templates_path(@project, @entity_type)
  end

  # Old version with multiple uploads
  # def create
  #   files = params[:template_files]
  #
  #   if files.present?
  #     errors = []
  #
  #     files.each do |file|
  #       template = Template.new(
  #         file_name:     file.original_filename,
  #         template_file: file,
  #         entity_type:   @entity_type
  #       )
  #       errors << file.original_filename unless template.save
  #     end
  #
  #     if errors.present?
  #       flash[:error] = "Error! #{error_count} not added!"
  #     else
  #       flash[:success] = 'Templates added'
  #     end
  #
  #     redirect_to @project
  #   else
  #     flash[:error] = "No files provided"
  #     render :new
  #   end
  # end
end
