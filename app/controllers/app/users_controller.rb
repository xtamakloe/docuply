class App::UsersController < App::BaseController
  before_action :set_user

  def show
  end


  def edit
  end


  def update_password
    if @user.update_with_password(user_params)
      flash[:success] = "Updated!"
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to user_path
    else
      render "edit"
    end
  end

  private

  def user_params
    # NOTE: Using `strong_parameters` gem
    params.require(:user).permit(
        :first_name,
        :last_name,
        :password,
        :password_confirmation,
        :current_password,
    )
  end

  def set_user
    @user = current_user
  end
end
