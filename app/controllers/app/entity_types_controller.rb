class App::EntityTypesController < App::BaseController

  before_action :load_project #, only: [:show, :edit, :create, :update, :new]

  def show
  end


  def edit
    render 'new'
  end


  def new
    @entity_type = EntityType.new
  end


  def create
    @entity_type = @project.entity_types.new(entity_type_params)
    if @entity_type.save
      flash[:success] = "Added!"
      redirect_to @project
    else
      flash.now[:error] = @entity_type.errors.full_messages.first
      render 'new'
    end
  end


  def update
    if @entity_type.update_attributes(entity_type_params)
      flash[:success] = "Updated!"
      redirect_to [@project, @entity_type]
    else
      flash.now[:error] = @entity_type.errors.full_messages.first
      render 'new'
    end
  end


  private


  def load_project
    @project     = current_account.projects.find(params[:project_id])
    @entity_type = @project.sole_entity_type
  end


  def entity_type_params
    params.require(:entity_type).permit(:name)
  end
end

