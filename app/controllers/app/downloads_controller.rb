# TODO: Needs heavy refactoring
class App::DownloadsController < App::BaseController
  before_action :load_operation


  # Download template-based documents
  def create
    entity_ids = params[:entity_ids]
    template_ids = params[:template_ids]
    output_type = params[:output_type]

    respond_to do |format|
      format.zip do
        download_file = generate_download_file(@operation, entity_ids, template_ids, output_type)

        send_file download_file,
                  type: 'application/zip',
                  disposition: 'attachment'
      end
    end

    #TODO:  Remove download files
  end


  private


  def load_operation
    @project = current_account.projects.find(params[:project_id])
    @operation = @project.operations.find(params[:operation_id])
  end


  def generate_download_file(operation, entity_ids, template_ids, output_type)
    entities = operation.entities.where(id: entity_ids)
    templates = operation.templates.where(id: template_ids)

    # Generate output files from templates
    # [{}, {}, ...]
    files =
        if output_type.to_sym == :file
          generate_docx_files(entities, templates)
        elsif output_type.to_sym == :text
          generate_xlsx_files(entities, templates)
        else
        end

    zip_files(files, template_ids.join) if files.present? #TODO: handle no files
  end


  # fill template
  # stick in array
  # return docx array
  def generate_docx_files(entities, templates)
    files = []

    if entities.present? && templates.present?
      entities.each do |entity|
        templates.each do |template|
          files << fill_template(template, entity)
        end
      end
    end

    files
  end


  # fill template
  # get text content of template
  # insert into excel file
  # convert to tmp_file
  # stick in array
  # Returns xlsx array
  def generate_xlsx_files(entities, templates)
    filename = "#{SecureRandom.uuid}.xlsx"
    workbook = FastExcel.open("#{Rails.root}/tmp/#{filename}", constant_memory: true)
    worksheet = workbook.add_worksheet('Sheet 1')
    worksheet.append_row(["Results"])

    begin
      files = []
      if entities.present? && templates.present?
        entities.each do |entity|
          templates.each do |template|
            files << fill_template(template, entity)
          end
        end
      end

      files.each do |f|
        worksheet.append_row([DocRipper::rip(f[:file].path)])
      end

      tmp_file = Tempfile.new(['wkbk_tmp_file', '.xlsx'], "#{Rails.root}/tmp", encoding: 'ascii-8bit')
      tmp_file.write(workbook.read_string)

    ensure
      # clean up
      # workbook.close # causes server to crash??? just delete file for now
      workbook.remove_tmp_file
      f_path = "#{Rails.root}/tmp/#{filename}"
      File.delete(f_path) if File.exists?(f_path)

      files.each { |f| f[:file].close; f[:file].unlink }
    end

    [{name: filename, file: tmp_file}]
  end


  # Return hash of filename and file { name: String, file: File}
  def fill_template(template, entity)
    begin
      # set final file name

      filename = "#{entity.uid}#{template.uid}-#{template.name}.docx"

      # get template and fill with data
      aws_tempfile = Util.get_aws_file(template.template_file.path, '.docx')
      sablon_template = Sablon.template(aws_tempfile.path)
      context = {}
      entity.entity_attributes.each do |entity_attribute|
        var_name = entity_attribute.var_name
        value = entity_attribute.value.presence || 'No data'
        context[var_name.to_sym] = value

        filename = "#{value}.docx" if var_name == "filename" # Update filename if user has added 'filename' field
      end

      tmp_file = Tempfile.new(['fill_tmp_file', '.docx'], "#{Rails.root}/tmp")

      sablon_template.render_to_file tmp_file, context

    ensure
      # clean up tempfile
      if aws_tempfile
        aws_tempfile.close; aws_tempfile.unlink
      end
    end

    {name: filename, file: tmp_file}
  end


  # files: [{}, {}, ...]
  def zip_files(files, output_filename)
    zipfile_name = "#{Rails.root}/tmp/#{output_filename}-#{SecureRandom.uuid}.zip"

    begin
      # Archive files
      Zip::File.open(zipfile_name, Zip::File::CREATE) do |zipfile|
        files.each do |f|
          zipfile.add(f[:name], f[:file].path)
        end
      end
    ensure
      # clean up
      files.each { |f| f[:file].close; f[:file].unlink }
    end

    zipfile_name
  end
end
