class App::OperationsController < App::BaseController

  before_action :load_entity_type


  def index
    @operations = @entity_type.operations.order(created_at: :desc)
  end


  def show
    # display links to allow documents to be downloaded
    # show entities
    # entity_types
    @operation = @entity_type.operations.find(params[:id])
  end


  def new
    @operation = Operation.new
  end


  def create
    # create operation that ties
    # entity, template and csv file
    @operation             = @project.operations.new(operation_params)
    @operation.entity_type = @entity_type
    if @operation.save
      flash[:success] = "Operation complete!"
      redirect_to project_entity_type_operation_path(@project, @entity_type, @operation)
    else
      flash.now[:error] = @operation.errors.full_messages
      render 'new'
    end
  end


  def destroy
    @operation = @entity_type.operations.find(params[:id])
    if @operation.destroy
      flash[:success] = "#{@operation.id} removed!"
      redirect_to project_entity_type_operations_path(@project, @entity_type)
    else
      flash[:error] = "Unable to remove!"
      redirect_to project_entity_type_operation_path(@project, @entity_type, @operation)
    end
  end


  private
  def load_entity_type
    @project     = current_account.projects.find(params[:project_id])
    @entity_type = @project.entity_types.find(params[:entity_type_id])
  end


  def operation_params
    params.require(:operation).permit(
      :notes,
      :operation_data_file,
      :custom_filenames,
      template_ids: [],
    )
  end
end
