class App::PagesController < ApplicationController
  layout :login_status


  def home
  end


  def help
  end


  def file_not_found
    @code = '404'
    @main = 'Oops! Page not found'
    @sub = 'The page you are looking for does not exist or has been moved'
    render 'error'
  end


  def unprocessable
    @code = '422'
    @main = 'The change you wanted was rejected'
    @sub = "Maybe you tried to change something you didn't have access to."
    render 'error'
  end


  def internal_server_error
    @code = '500'
    @main = "We're sorry, but something went wrong."
    @sub = "If you are the application owner check the logs for more information."
    render 'error'
  end

  private

  def login_status
    current_user ? 'app' : 'sessions'
  end


end
