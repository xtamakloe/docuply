import * as $ from 'jquery';
import Masonry from 'masonry-layout';

export default (function () {
    document.addEventListener('turbolinks:load', () => {
    // $(document).ready(function(){

      if ($('.masonry').length > 0) {
        new Masonry('.masonry', {
          itemSelector: '.masonry-item',
          columnWidth: '.masonry-sizer',
          percentPosition: true,
        });
      }
    });
  // window.addEventListener('load', () => {
  //   });

}());
