import * as $ from 'jquery';

export default (function () {
  document.addEventListener('turbolinks:load', () => {
    // $(document).ready(function() {
    $('.search-toggle').on('click', e => {
      $('.search-box, .search-input').toggleClass('active');
      $('.search-input input').focus();
      e.preventDefault();
    });
  });


}());
