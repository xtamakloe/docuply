import * as $ from 'jquery';
import 'bootstrap';

export default (function () {

  document.addEventListener('turbolinks:load', () => {
  //   $(document).ready(function() {
    // ------------------------------------------------------
    // @Popover
    // ------------------------------------------------------

    $('[data-toggle="popover"]').popover();

    // ------------------------------------------------------
    // @Tooltips
    // ------------------------------------------------------

    $('[data-toggle="tooltip"]').tooltip();
  });
}());
