class Util

  def self.get_aws_file(path, filetype)
    key       = path
    bucket    = ENV['AWS_BUCKET']
    s3_object = Aws::S3::Resource.new.bucket(bucket).object(key)
    tempfile  = Tempfile.new(['aws_tmp_file', filetype], "#{Rails.root}/tmp")
    tempfile << s3_object.get.body.read
    tempfile
  end

end