# This is the attributes that a datatype/entity_type has
# i.e. attributes connected to a data type rather than a data point
class EntityTypeAttribute < ApplicationRecord
  belongs_to :entity_type
  has_many :entity_attributes, dependent: :destroy  # TODO: review implications
                                                    # of dependent: :destroy,
                                                    # should attribute be
                                                    # removed if type is
                                                    # deleted? I think not
                                                    # just a warning that the
                                                    # type doesn't exist anymore

  validates :var_name,
            presence:   true,
            uniqueness: { scope: :entity_type_id, message: 'duplicates present' }
end
