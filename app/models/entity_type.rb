# This is the underlying datatype of each record/row in the  upload file,
# e.g. Student, Trainee, Employee, etc.
# The datatype has attributes that each data point will have e.g. name, age
# Since app now uses only one datatype per project, this is not used
class EntityType < ApplicationRecord

  belongs_to :project
  has_many :entity_type_attributes, dependent: :destroy
  has_many :templates, dependent: :destroy
  has_many :operations, dependent: :destroy

  after_create :add_filename_attribute


  # def fields
  #   self.entity_type_attributes.map(&:var_name)
  # end
  #
  # def field_names
  #   self.entity_type_attributes.map(&:name)
  # end


  def filename_attribute_type
    self.entity_type_attributes.where(var_name: 'filename').first
  end


  def placeholder_attributes
    self.entity_type_attributes.where(hidden: false)
  end


  private

  # Create a filename attribute used to retrieve custom filename data from uplaod file
  def add_filename_attribute
    self.entity_type_attributes.create(
        name: 'Custom filename',
        var_name: 'filename',
        hidden: true,
    )
  end
end
