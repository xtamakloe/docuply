class Project < ApplicationRecord
  belongs_to :account
  has_many :entity_types, dependent: :destroy
  has_many :operations, dependent: :destroy

  validates :title, presence: true

  after_create :create_sole_entity_type


  def sole_entity_type
    self.entity_types.first
  end


  private


  def create_sole_entity_type
    self.entity_types.create(name: "data-type-#{self.id}")
  end
end

