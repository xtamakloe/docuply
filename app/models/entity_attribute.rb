# This is the attribute of a data point (Entity)
class EntityAttribute < ApplicationRecord

  belongs_to :entity
  belongs_to :entity_type_attribute


  # Get name and var_name from EntityAttributeType (can be modified centrally)
  def var_name
    self.entity_type_attribute.var_name
  end


  def name
    self.entity_type_attribute.name
  end
end
