class Operation < ApplicationRecord

  mount_uploader :operation_data_file, OperationDataFileUploader

  belongs_to :project
  belongs_to :entity_type
  has_and_belongs_to_many :templates
  has_many :entities, dependent: :destroy

  after_create :create_entities


  def uid
    "OP-#{self.id}"
  end


  def uid_for_entity(entity)
    "#{self.uid}-#{entity.uid}"
  end


  def entity_fields
    self.entity_type.fields
  end


  def template_names
    self.templates.map(&:name)
  end


  # class String
  #   def string_between_markers marker1, marker2
  #     self[/#{Regexp.escape(marker1)}(.*?)#{Regexp.escape(marker2)}/m, 1]
  #   end
  # end

  private


  # TODO: output errors to user
  def create_entities
    # grab entity_type
    # create an entity record
    # for each entity record,
    #   create an entity_attribute based on from entity_type_attribute info
    #   the entity_types entity_type_attributes and assign to entity

    require 'util'
    tempfile = Util.get_aws_file(self.operation_data_file.path, '.xlsx')

    data = Roo::Spreadsheet.open(tempfile.path, extension: 'xlsx')
    headers = data.row(1)

    data.each_with_index do |row, index|

      next if index == 0 # skip header  # create hash from headers and cells

      entity_data = Hash[[headers, row].transpose]

      # Create Entity record from EntityType
      entity = Entity.new(
          operation: self,
          project: self.project,
          entity_type: self.entity_type,
      )

      # Create EntityAttributes for the Entity
      # based on the EntityType's attributes (EntityTypeAttribue)
      if entity.save

        # Create filename attribute if user sets flag to use custom filenames
        if self.custom_filenames
          f_var_name = entity.entity_type.filename_attribute_type.var_name
          filename = entity_data[f_var_name]

          if filename.present?
            EntityAttribute.create(
                entity_type_attribute: entity.entity_type.filename_attribute_type,
                entity: entity,
                value: entity_data[f_var_name] # name of upload column
            )
          else
            puts 'ERRORS: Custom filenames not present'
          end
        end

        self.entity_type.placeholder_attributes.each do |et_attribute|
          entity_attribute = EntityAttribute.new(
              entity_type_attribute: et_attribute,
              entity: entity,
              value: entity_data[et_attribute.var_name]
          )
          # byebug
          unless entity_attribute.save
            puts 'ERRORS: entity_attribute not saved'
            entity_attribute.errors
          end
        end
      else
        puts 'ERRORS: entity not saved'
        puts entity.errors
      end

      # Remove uploaded file (save record needed)
      self.remove_operation_data_file!
      self.save
      self.reload

      # Clear up temp file
      tempfile.close
      tempfile.unlink
    end
  end
end
