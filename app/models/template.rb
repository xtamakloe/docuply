class Template < ApplicationRecord
  mount_uploader :template_file, TemplateFileUploader

  belongs_to :entity_type

  # NB: No need for removing dependent operations
  # since they might have other templates connected
  has_and_belongs_to_many :operations

  before_save :set_filename
  before_save :set_placeholders_list

  def uid
    "T#{self.id}"
  end


  def registered_placeholders
    self.registered_placeholders_list.split('|')
  end


  def all_placeholders
    self.all_placeholders_list.split('|')
  end


  def set_placeholders_list
    fields_list = self.retrieve_fields

    all_list = fields_list[:all_fields]
    reg_list = fields_list[:registered_fields]

    self.registered_placeholders_list = reg_list.join('|') if reg_list.present?
    self.all_placeholders_list = all_list.join('|') if all_list.present?
  end


  def retrieve_fields
    require 'util'
    tempfile = Util.get_aws_file(self.template_file.path, '.docx')
    document_string = DocRipper::rip(tempfile.path)
    tempfile.close
    tempfile.unlink

    all_fields_array = document_string.scan(/(p_\w*)/).uniq.flatten

    registered_fields_array = []
    var_names_array = self.entity_type.placeholder_attributes.map(&:var_name) # get only unhidden attribtues => no filenames
    var_names_array.each do |var_name|
      registered_fields_array << var_name if document_string.include?(var_name)
    end
    registered_fields_array.compact.uniq!

    {all_fields: all_fields_array, registered_fields: registered_fields_array}
  end


  private


  def set_filename
    self.file_name = self.template_file.path.split('/').last
  end
end
