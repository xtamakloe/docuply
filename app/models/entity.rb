# This represents a data point (i.e. records/rows found in upload data)
class Entity < ApplicationRecord

  belongs_to :project
  belongs_to :entity_type
  belongs_to :operation
  has_many :entity_attributes, dependent: :destroy


  def uid
    "E#{self.id}"
  end

  # Get name from entity_type, so can be modified centrally
  # entity.entity_type.name
  def name
    self.entity_type.name
  end


  def templates_from_entity_type
    self.entity_type.templates
  end


  def templates_from_operation
    self.operation.templates
  end


  def selected_templates_from(operation)
    # templates for entity
    # templates for operation
    # where they intersect
    operation.templates.where(id: templates_from_entity_type.map(&:id))
  end


  def var_name
    self.entity_type_attribute.var_name
  end


end
